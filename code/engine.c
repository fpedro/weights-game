#include "game.h"

double scaleFactorX = 0;
double scaleFactorY = 0;

void mustInit(bool test, const char *description) {
   if(test) return;

   printf("couldn't initialize %s\n", description);
   exit(1);
}

ALLEGRO_DISPLAY *initDisplay(short w, short h) {
   ALLEGRO_DISPLAY *d = al_create_display(w, h);
   mustInit(d, "display");

   scaleFactorX = ((float)al_get_display_width(d)) / w;
   scaleFactorY = ((float)al_get_display_height(d)) / h;

   ALLEGRO_TRANSFORM t;
   
   al_identity_transform(&t);
   al_scale_transform(&t, scaleFactorX, scaleFactorY);
   al_use_transform(&t);
   
   return d;
}

short getMouseX(ALLEGRO_EVENT *e) {
   return e->mouse.x / scaleFactorX;
}

short getMouseY(ALLEGRO_EVENT *e) {
   return e->mouse.y / scaleFactorY;
}
