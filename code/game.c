#include "game.h"

solution solutionWeight;
weight WEIGHT_OPTION[4];

int main(int argc, char *argv[]) {

   weight *weights;
   short balanceTotal = 0;
   char balanceState = LITTLE_WEIGHT;
   weight *selectedWeight = NULL;
   weight *w;
   weight *wAux;
   char figureIndex = -1;
   size solutionSize;
   size s;
   size anchorSize;   
   short moveBottom;
   char option;
   
   printf("Allegro version: ");
   printf("%d\n\n", al_get_allegro_version());
   mustInit(al_init(), "allegro");
   mustInit(al_install_keyboard(), "keyboard");
   mustInit(al_install_mouse(), "mouse");
   mustInit(al_init_image_addon(), "image addon");
   mustInit(al_init_native_dialog_addon(), "native dialog addon");

   if (argc > 1) option = *argv[1];
   else option = 'a';

   setSolution(&solutionWeight, option);
   solutionSize = getSolutionSize(option);

   WEIGHT_OPTION[0].weight = 1;
   WEIGHT_OPTION[0].positionX = 390;
   WEIGHT_OPTION[0].positionY = 675;
   
   WEIGHT_OPTION[1].weight = 4;
   WEIGHT_OPTION[1].positionX = 305;
   WEIGHT_OPTION[1].positionY = 665;

   WEIGHT_OPTION[2].weight = 6;
   WEIGHT_OPTION[2].positionX = 215;
   WEIGHT_OPTION[2].positionY = 660;

   WEIGHT_OPTION[3].weight = 9;
   WEIGHT_OPTION[3].positionX = 115;
   WEIGHT_OPTION[3].positionY = 650;

   short mouseStartX = 0,
         mouseEndX = 0,
         mouseStartY = 0,
         mouseEndY = 0;
   bool buildNewWeight = false;
   bool quitGame = false;
   
   ALLEGRO_BITMAP* balanceEquilibrium_figure;
   ALLEGRO_BITMAP* balanceLittleWeight_figure;
   ALLEGRO_BITMAP* balanceBigWeight_figure;
   ALLEGRO_BITMAP* weight_figure[4];
   ALLEGRO_BITMAP* weight_highlightFigure[4];

         
   short mouseX = 0, mouseY = 0;
   bool mouseButtonPressed = false;

  
   balanceEquilibrium_figure = al_load_bitmap(
     FIGURES_PATH "balance.png");
   balanceLittleWeight_figure = al_load_bitmap(
     FIGURES_PATH "balance_little-weight.png");
   balanceBigWeight_figure = al_load_bitmap(
     FIGURES_PATH "balance_big-weight.png");
   weight_figure[0] = al_load_bitmap(FIGURES_PATH "weight1.png");
   weight_highlightFigure[0] = al_load_bitmap(FIGURES_PATH "weight1_highlight.png");
   weight_figure[1] = al_load_bitmap(FIGURES_PATH "weight4.png");
   weight_highlightFigure[1] = al_load_bitmap(FIGURES_PATH "weight4_highlight.png");
   weight_figure[2] = al_load_bitmap(FIGURES_PATH "weight6.png");
   weight_highlightFigure[2] = al_load_bitmap(FIGURES_PATH "weight6_highlight.png");
   weight_figure[3] = al_load_bitmap(FIGURES_PATH "weight9.png");
   weight_highlightFigure[3] = al_load_bitmap(FIGURES_PATH "weight9_highlight.png");

   ALLEGRO_TIMER* timer = al_create_timer(1.0 / 30.0);
   mustInit(timer, "timer");

   ALLEGRO_EVENT_QUEUE* queue = al_create_event_queue();
   mustInit(queue, "queue");

//   al_set_new_display_option(ALLEGRO_SAMPLE_BUFFERS, 1, ALLEGRO_SUGGEST);
//   al_set_new_display_option(ALLEGRO_SAMPLES, 8, ALLEGRO_SUGGEST);
//   al_set_new_bitmap_flags(ALLEGRO_MIN_LINEAR | ALLEGRO_MAG_LINEAR);

   al_set_new_display_flags(ALLEGRO_FULLSCREEN_WINDOW);

   ALLEGRO_DISPLAY* display;
   
   display = initDisplay(FRAME_WIDTH, FRAME_HEIGHT);

   ALLEGRO_FONT* font = al_create_builtin_font();
   mustInit(font, "font");

   mustInit(al_init_primitives_addon(), "primitives");


   al_register_event_source(queue, al_get_keyboard_event_source());
   al_register_event_source(queue, al_get_mouse_event_source());
   al_register_event_source(queue, al_get_display_event_source(display));
   al_register_event_source(queue, al_get_timer_event_source(timer));

   bool redraw = true;

   ALLEGRO_EVENT event;

   float x, y;
   x = 100;
   y = 100;


   al_set_window_title(display, "Weights Game");
   al_start_timer(timer);

   bool timerEvent = true;

   while (1) {
  
      al_wait_for_event(queue, &event);
      switch (event.type)
      {
         case ALLEGRO_EVENT_TIMER:
            if (buildNewWeight) {
               w = (weight*)malloc(sizeof(weight));
               w->weight = selectedWeight->weight;
               w->positionX = selectedWeight->positionX;
               w->positionY = selectedWeight->positionY;
               w->anchor = NULL;
               w->nextWeight = NULL;
               if (weights == NULL)
                  weights = w;
               else {
                  wAux = weights;
                  while (wAux->nextWeight != NULL)
                     wAux = wAux->nextWeight;
                  wAux->nextWeight = w;
               }
               selectedWeight = w;
               buildNewWeight = false;
            }
            if (selectedWeight != NULL) {
               short moveX = 0,
               moveY = 0;
               moveX = mouseEndX - mouseStartX;
               moveY = mouseEndY - mouseStartY;
               selectedWeight->positionX += moveX;
               selectedWeight->positionY += moveY;
               wAux = weights;
               for (; wAux != NULL; wAux = wAux->nextWeight) {
                  if (isOverBalance(wAux) &&
                      wAux != selectedWeight) {
                      wAux->anchor = NULL;
                      s = getSize(wAux);
                     for (weight *i = weights; i != NULL; i = i->nextWeight) {
                        if (i == selectedWeight ||
                            !isOverBalance(i)) continue;
                        anchorSize = getSize(i);
                        if ((wAux->positionX >= i->positionX &&
                              wAux->positionX <= getRightSide(i)) ||
                            (getRightSide(wAux) >= i->positionX &&
                              getRightSide(wAux) <= getRightSide(i)) ||
                            s.width > anchorSize.width && 
                            ((wAux->positionX +
                               s.width / 2) >= i->positionX &&
                             (wAux->positionX +
                               s.width / 2) <= getRightSide(i))) {
                           if (getBottom(wAux) <= i->positionY) {
                                 if (wAux->anchor == NULL)
                                    wAux->anchor = i;
                                 else {
                                    if (wAux->anchor->positionY > i->positionY)
                                       wAux->anchor = i;
                                 }
                           }
                        } 
                     }
                     if (wAux->anchor == NULL)
                        wAux->anchor = wAux;
                  }
               }
               wAux = weights;
               for (; wAux != NULL; wAux = wAux->nextWeight) {
                  if (wAux->anchor == NULL ||
                      wAux == selectedWeight ||
                      !isOverBalance(wAux)) continue;
                  s = getSize(wAux);
                  if (wAux->anchor == wAux)
                     wAux->positionY = getBalanceBottom() - s.height;
                  else
                     wAux->positionY = wAux->anchor->positionY - s.height;    
               }      
                                    
            }
            else {
               if (w != NULL) {
                  bool sobrebalance = false;
                  weight *currentBottom = NULL;
                  s = getSize(w);     
                  if (isOverBalance(w)) {
                     wAux = weights;
                     for (; wAux != NULL; wAux = wAux->nextWeight) {
                        anchorSize = getSize(wAux);
                        if (isOverBalance(wAux) &&
                            ((w->positionX >= wAux->positionX &&
                              w->positionX <= getRightSide(wAux)) ||
                            (getRightSide(w) >= wAux->positionX &&
                              getRightSide(w) <= getRightSide(wAux)) ||
                            s.width > anchorSize.width &&
                            ((w->positionX +
                               s.width / 2) >= wAux->positionX &&
                            (w->positionX +
                               s.width / 2) <= getRightSide(wAux)))) {
                           if (getBottom(w) <= wAux->positionY) {
                              if (currentBottom == NULL)
                                 currentBottom = wAux;
                              else {
                                 if (currentBottom->positionY > wAux->positionY)
                                    currentBottom = wAux; 
                              }
                           }
                        } 
                     }
                     if (currentBottom == NULL)
                        w->positionY = getBalanceBottom() - s.height;
                     else {
                        w->positionY = currentBottom->positionY - s.height;
                     }
                  }
                  w = NULL;
               }
            }
            balanceTotal = 0;
            short newBalanceBottom;
            wAux = weights;
            for (; wAux != NULL; wAux = wAux->nextWeight) {
               if (isOverBalance(wAux) &&
                   wAux != selectedWeight)
                  balanceTotal += wAux->weight;
            }
            if (balanceTotal == solutionWeight.properties.weight) {
               balanceState = EQUILIBRIUM;
               newBalanceBottom = BOTTOM_EQUILIBRIUM;
            }
            else if (balanceTotal > solutionWeight.properties.weight) {
               balanceState = BIG_WEIGHT;
               newBalanceBottom = VARIABLE_BOTTOM_BIG_WEIGHT;
            }
            else {
               balanceState = LITTLE_WEIGHT;
               newBalanceBottom = VARIABLE_BOTTOM_LITTLE_WEIGHT;
            }
            moveBottom = getBalanceBottom() - newBalanceBottom;
            wAux = weights;
            if (moveBottom != 0) {
               for (; wAux != NULL; wAux = wAux->nextWeight) {
                  if (isOverBalance(wAux) &&
                      wAux != selectedWeight)
                     wAux->positionY -= moveBottom;
               }            
            }
            setBalanceBottom(newBalanceBottom);
            mouseStartX = mouseEndX;
            mouseStartY = mouseEndY;
            redraw = true;
            break;
         case ALLEGRO_EVENT_MOUSE_AXES:
            mouseX = getMouseX(&event);
            mouseY = getMouseY(&event);
            mouseEndX = mouseX;
            mouseEndY = mouseY;
            break;
         case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:
            mouseX = getMouseX(&event);
            mouseY = getMouseY(&event);
            mouseButtonPressed = true;
            //al_append_native_text_log(logWindow, "button pressed\n\nmouseX: %d, mouseY: %d\n", mouseX, mouseY);
            for (int i = 0; i < 4; i++) {
               if (mouseOverWeight(mouseX, mouseY, &WEIGHT_OPTION[i])) {
                  selectedWeight = &WEIGHT_OPTION[i];
                  buildNewWeight = true;
                  break;
               }               
            }
            wAux = weights;
            for (; wAux != NULL; wAux = wAux->nextWeight) {
               if (mouseOverWeight(mouseX, mouseY, wAux)) {
                  selectedWeight = wAux;
                  buildNewWeight = false;
               }               
            }
            w = selectedWeight;            
            mouseStartX = mouseX;
            mouseStartY = mouseY;
            break;
         case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
            mouseButtonPressed = false;
            selectedWeight = NULL;
         case ALLEGRO_EVENT_KEY_DOWN:
            if (event.keyboard.keycode == ALLEGRO_KEY_Q)
               quitGame = true;
            break;
         case ALLEGRO_EVENT_DISPLAY_CLOSE:
            quitGame = true;
            break;
      }

      if(quitGame)
         break;

      if(redraw && al_is_event_queue_empty(queue))
      {
         al_clear_to_color(al_map_rgb(255, 255, 255));
//         al_draw_textf(font, al_map_rgb(0, 0, 0), 0, 0, 0, "total da balança: %d\n", balanceTotal);
         switch (balanceState) {
            case EQUILIBRIUM:
               al_draw_bitmap(balanceEquilibrium_figure, 230, 65, 0);
               al_draw_bitmap(solutionWeight.figure,
                 solutionWeight.properties.positionX, BOTTOM_EQUILIBRIUM - solutionSize.height, 0);
               break;
            case BIG_WEIGHT:
               al_draw_bitmap(balanceBigWeight_figure, 230, 65, 0);
               al_draw_bitmap(solutionWeight.figure,
                 solutionWeight.properties.positionX, SOLUTION_BOTTOM_BIG_WEIGHT - solutionSize.height, 0);
               break;
            case LITTLE_WEIGHT:
               al_draw_bitmap(balanceLittleWeight_figure, 230, 65, 0);
               al_draw_bitmap(solutionWeight.figure,
                 solutionWeight.properties.positionX, SOLUTION_BOTTOM_LITTLE_WEIGHT - solutionSize.height,0);
               break;
         }
         for (int i = 0; i < 4; i ++) {
            al_draw_bitmap(weight_figure[i], WEIGHT_OPTION[i].positionX, WEIGHT_OPTION[i].positionY, 0);
         }
         wAux = weights;
         for (; wAux != NULL; wAux = wAux->nextWeight) {
            figureIndex = getFigureIndex(wAux);
            if (selectedWeight == wAux)
               al_draw_bitmap(weight_highlightFigure[figureIndex],
                                wAux->positionX, wAux->positionY, 0);
            else 
               al_draw_bitmap(weight_figure[figureIndex],
                                wAux->positionX, wAux->positionY, 0);
         }
         if (selectedWeight != NULL) {         
            figureIndex = getFigureIndex(selectedWeight);
            al_draw_bitmap(weight_highlightFigure[figureIndex],
              selectedWeight->positionX, selectedWeight->positionY, 0);
         }
         al_flip_display();
         redraw = false;
      }
   }
   
   wAux = weights;
   while (wAux != NULL) {
      wAux = wAux->nextWeight;
      free (weights);
      weights = wAux;
   }

   al_destroy_bitmap(solutionWeight.figure);
   al_destroy_bitmap(balanceEquilibrium_figure);
   al_destroy_bitmap(balanceBigWeight_figure);
   al_destroy_bitmap(balanceLittleWeight_figure);
   for (int i = 0; i < 4; i ++) {
      al_destroy_bitmap(weight_figure[i]);
      al_destroy_bitmap(weight_highlightFigure[i]);
   }
   al_destroy_font(font);
   al_destroy_display(display);
   al_destroy_timer(timer);
   al_destroy_event_queue(queue);

   return 0;
}
