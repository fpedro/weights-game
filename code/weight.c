#include "game.h"

short balanceBottom = VARIABLE_BOTTOM_LITTLE_WEIGHT;

short getBalanceBottom() {
   return balanceBottom;
}

void setBalanceBottom(short b) {
   balanceBottom = b;
}

size getSize(weight *p) {
   size s;
   switch (p->weight) {
      case 1:
         s.width = WEIGHT_1_WIDTH;
         s.height = WEIGHT_1_HEIGHT;
         break;
      case 4:
         s.width = WEIGHT_4_WIDTH;
         s.height = WEIGHT_4_HEIGHT;
         break;
      case 6:
         s.width = WEIGHT_6_WIDTH;
         s.height = WEIGHT_6_HEIGHT;
         break;
      case 9:
         s.width = WEIGHT_9_WIDTH;
         s.height = WEIGHT_9_HEIGHT;
         break;      
   }
   return s;
}

short getRightSide(weight *p) {
   size s = getSize(p);
   return s.width + p->positionX;
}

short getBottom(weight *p) {
   size s = getSize(p);
   return s.height + p->positionY;
}

char getSolutionIndex(char option) {
   return 'a' - option;
}

char getFigureIndex(weight *p) {
   switch (p->weight) {
      case 1: return 0;
      case 4: return 1;
      case 6: return 2;
      case 9: return 3;            
   }
}

bool isOverBalance(weight *p) {
   size s = getSize(p);     
   return (p->positionY + s.height <= balanceBottom &&
           (getRightSide(p) >= BALANCE_BOTTOM_START &&
           p->positionX <= BALANCE_BOTTOM_END));
}

bool mouseOverWeight(short x, short y, weight *p) {
   size s = getSize(p);
   if (x >= p->positionX && x <= p->positionX + s.width &&
       y >= p->positionY && y <= p->positionY + s.height)
      return true;
   return false;
}
