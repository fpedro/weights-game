#include "game.h"

void setSolution(solution *s, char option) {
   switch (option) {
      case 'a':
         s->properties.weight = 4;
         s->properties.positionX = 280;
         s->figure = al_load_bitmap(FIGURES_PATH "weight-a.png");
         break;
      case 'b':
         s->properties.weight = 1;
         s->properties.positionX = 310;
         s->figure = al_load_bitmap(FIGURES_PATH "weight-b.png");
         break;
      case 'c':
         s->properties.weight = 8;
         s->properties.positionX = 310;
         s->figure = al_load_bitmap(FIGURES_PATH "weight-c.png");
         break;
      case 'd':
         s->properties.weight = 11;
         s->properties.positionX = 310;
         s->figure = al_load_bitmap(FIGURES_PATH "weight-d.png");
         break;
      case 'e':
         s->properties.weight = 14;
         s->properties.positionX = 310;
         s->figure = al_load_bitmap(FIGURES_PATH "weight-e.png");
         break;
      case 'f':
         s->properties.weight = 9;
         s->properties.positionX = 310;
         s->figure = al_load_bitmap(FIGURES_PATH "weight-f.png");
         break;
      case 'g':
         s->properties.weight = 21;
         s->properties.positionX = 310;
         s->figure = al_load_bitmap(FIGURES_PATH "weight-g.png");
         break;
      case 'h':
         s->properties.weight = 15;
         s->properties.positionX = 310;
         s->figure = al_load_bitmap(FIGURES_PATH "weight-h.png");
         break;
      case 'i':
         s->properties.weight = 25;
         s->properties.positionX = 310;
         s->figure = al_load_bitmap(FIGURES_PATH "weight-i.png");
         break;
      case 'j':
         s->properties.weight = 30;
         s->properties.positionX = 310;
         s->figure = al_load_bitmap(FIGURES_PATH "weight-j.png");
         break;
      case 'k':
         s->properties.weight = 34;
         s->properties.positionX = 310;
         s->figure = al_load_bitmap(FIGURES_PATH "weight-k.png");
         break;
      case 'l':
         s->properties.weight = 17;
         s->properties.positionX = 310;
         s->figure = al_load_bitmap(FIGURES_PATH "weight-l.png");
         break;
      case 'm':
         s->properties.weight = 51;
         s->properties.positionX = 310;
         s->figure = al_load_bitmap(FIGURES_PATH "weight-m.png");
         break;         
   }
}

size getSolutionSize(char option) {
   size s;
   switch (option) {
      case 'a':
         s.width = WEIGHT_A_WIDTH;
         s.height = WEIGHT_A_HEIGHT;
         break;
      case 'b':
         s.width = WEIGHT_B_WIDTH;
         s.height = WEIGHT_B_HEIGHT;
         break;
      case 'c':
         s.width = WEIGHT_C_WIDTH;
         s.height = WEIGHT_C_HEIGHT;
         break;
      case 'd':
         s.width = WEIGHT_D_WIDTH;
         s.height = WEIGHT_D_HEIGHT;
         break;
      case 'e':
         s.width = WEIGHT_E_WIDTH;
         s.height = WEIGHT_E_HEIGHT;
         break;
      case 'f':
         s.width = WEIGHT_F_WIDTH;
         s.height = WEIGHT_F_HEIGHT;
         break;
      case 'g':
         s.width = WEIGHT_G_WIDTH;
         s.height = WEIGHT_G_HEIGHT;
         break;
      case 'h':
         s.width = WEIGHT_H_WIDTH;
         s.height = WEIGHT_H_HEIGHT;
         break;
      case 'i':
         s.width = WEIGHT_I_WIDTH;
         s.height = WEIGHT_I_HEIGHT;
         break;
      case 'j':
         s.width = WEIGHT_J_WIDTH;
         s.height = WEIGHT_J_HEIGHT;
         break;
      case 'k':
         s.width = WEIGHT_K_WIDTH;
         s.height = WEIGHT_K_HEIGHT;
         break;
      case 'l':
         s.width = WEIGHT_L_WIDTH;
         s.height = WEIGHT_L_HEIGHT;
         break;
      case 'm':
         s.width = WEIGHT_M_WIDTH;
         s.height = WEIGHT_M_HEIGHT;
         break;
   }
   return s;
}

