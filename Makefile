VPATH = code:include

all: game.c weight.o engine.o solution.o game.h weight.h
	gcc code/weight.o code/engine.o code/solution.o code/game.c -o game -I/usr/include/x86_64-linux-gnu -Iinclude -lallegro_primitives -lallegro_acodec -lallegro_audio -lallegro_image -lallegro_ttf -lallegro_font -lallegro_dialog -lallegro

solution.o: solution.c engine.o weight.o game.h weight.h
	gcc -c code/solution.c -Iinclude -o code/solution.o

engine.o: engine.c weight.o game.h weight.h
	gcc -c code/engine.c -Iinclude -o code/engine.o

weight.o: weight.c game.h weight.h
	gcc -c code/weight.c -Iinclude -o code/weight.o

clean:
	rm game code/weight.o code/engine.o code/solution.o
