#include <stdio.h>
#include <stdlib.h>

#include <allegro5/allegro5.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_native_dialog.h>
#include <allegro5/allegro_primitives.h>

#include "weight.h"
#include "engine.h"

#define FIGURES_PATH "figures/"

#define FRAME_WIDTH                 1366
#define FRAME_HEIGHT  	             768

#define BALANCE_X                    230
#define BALANCE_Y                     65

#define BALANCE_WIDTH                800
#define BALANCE_HEIGHT               665

#define BOTTOM_EQUILIBRIUM  400 + BALANCE_Y

#define VARIABLE_BOTTOM_LITTLE_WEIGHT  275 + BALANCE_Y
#define VARIABLE_BOTTOM_BIG_WEIGHT     521 + BALANCE_Y

#define SOLUTION_BOTTOM_LITTLE_WEIGHT  VARIABLE_BOTTOM_BIG_WEIGHT
#define SOLUTION_BOTTOM_BIG_WEIGHT  VARIABLE_BOTTOM_LITTLE_WEIGHT

#define BALANCE_BOTTOM_START           450 + BALANCE_X
#define BALANCE_BOTTOM_END  BALANCE_BOTTOM_START + 337

#define EQUILIBRIUM                    1
#define BIG_WEIGHT                     2
#define LITTLE_WEIGHT                  3

typedef struct _weight weight;
typedef struct _size size;
typedef struct _solution solution;

struct _weight {
   short positionX;
   short positionY;
   short weight;
   weight *anchor;
   weight *nextWeight;
};

struct _size {
   short width;
   short height;
};

struct _solution {
   weight properties;
   ALLEGRO_BITMAP *figure;
};

void setSolution(solution *, char);
size getSize(weight *);
size getSolutionSize(char);
char getFigureIndex(weight *);
bool mouseOverWeight(short, short, weight *);
bool isOverBalance(weight *);
short getRightSide(weight *);
short getBottom(weight *);
