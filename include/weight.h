#define WEIGHT_1_WIDTH                   55
#define WEIGHT_1_HEIGHT                  55

#define WEIGHT_4_WIDTH                   65
#define WEIGHT_4_HEIGHT                  65

#define WEIGHT_6_WIDTH                   70
#define WEIGHT_6_HEIGHT                  70

#define WEIGHT_9_WIDTH                   80
#define WEIGHT_9_HEIGHT                  80


#define WEIGHT_A_WIDTH                  240 
#define WEIGHT_A_HEIGHT                 168

#define WEIGHT_B_WIDTH                  240 
#define WEIGHT_B_HEIGHT                 136

#define WEIGHT_C_WIDTH                  240 
#define WEIGHT_C_HEIGHT                 111

#define WEIGHT_D_WIDTH                  240 
#define WEIGHT_D_HEIGHT                 121

#define WEIGHT_E_WIDTH                  240 
#define WEIGHT_E_HEIGHT                 217

#define WEIGHT_F_WIDTH                  240 
#define WEIGHT_F_HEIGHT                 135

#define WEIGHT_G_WIDTH                  240 
#define WEIGHT_G_HEIGHT                 123

#define WEIGHT_H_WIDTH                  240 
#define WEIGHT_H_HEIGHT                  94

#define WEIGHT_I_WIDTH                  108 
#define WEIGHT_I_HEIGHT                 102

#define WEIGHT_J_WIDTH                  240 
#define WEIGHT_J_HEIGHT                 139

#define WEIGHT_K_WIDTH                  240 
#define WEIGHT_K_HEIGHT                 171

#define WEIGHT_L_WIDTH                  120 
#define WEIGHT_L_HEIGHT                  85

#define WEIGHT_M_WIDTH                  240 
#define WEIGHT_M_HEIGHT                 127


short getBalanceBottom();
void setBalanceBottom(short);

